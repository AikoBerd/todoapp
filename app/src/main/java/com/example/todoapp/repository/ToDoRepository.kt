package com.example.todoapp.repository

import com.example.todoapp.helper.DatabaseHelper
import com.example.todoapp.model.ToDoItem
import com.example.todoapp.model.ToDoList

class ToDoRepository(private val dbHelper: DatabaseHelper) {

    fun getAllToDoLists(): List<ToDoList> {
        return dbHelper.getAllToDoLists()
    }

    fun addList(name: String) {
        val newList = ToDoList(name = name)
        dbHelper.addList(newList)
    }

    fun deleteList(list: ToDoList) {
        dbHelper.deleteList(list)
    }

    fun updateList(list: ToDoList) {
        dbHelper.updateList(list)
    }

    fun getAllToDoItems(listId: Long): List<ToDoItem> {
        return dbHelper.getAllToDoItems(listId)
    }

    fun addItem(name: String, completed: Boolean, listId: Long) {
        val newItem = ToDoItem(name = name, completed = completed, listId = listId)
        dbHelper.addItem(newItem)
    }

    fun deleteItem(list: ToDoItem) {
        dbHelper.deleteItem(list)
    }

    fun updateItem(list: ToDoItem) {
        dbHelper.updateItem(list)
    }

    fun getListNameById(listId: Long): String{
        return dbHelper.getListNameById(listId)
    }
}