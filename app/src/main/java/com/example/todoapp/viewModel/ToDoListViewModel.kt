package com.example.todoapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.todoapp.helper.DatabaseHelper
import com.example.todoapp.model.ToDoList
import com.example.todoapp.repository.ToDoRepository

class ToDoListViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ToDoRepository

    private val _allToDoLists = MutableLiveData<List<ToDoList>>()
    val allToDoLists: LiveData<List<ToDoList>> = _allToDoLists

    init {
        val dbHelper = DatabaseHelper(application)
        repository = ToDoRepository(dbHelper)
        _allToDoLists.postValue(repository.getAllToDoLists())
    }

    fun addList(name: String) {
        repository.addList(name)
        _allToDoLists.postValue(repository.getAllToDoLists())
    }

    fun deleteList(list: ToDoList) {
        repository.deleteList(list)
        _allToDoLists.postValue(repository.getAllToDoLists())
    }

    fun updateList(list: ToDoList) {
        repository.updateList(list)
        _allToDoLists.postValue(repository.getAllToDoLists())
    }
}