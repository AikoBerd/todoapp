package com.example.todoapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.todoapp.helper.DatabaseHelper
import com.example.todoapp.model.ToDoItem
import com.example.todoapp.repository.ToDoRepository

class ToDoItemViewModel(application: Application, private val listId: Long) : AndroidViewModel(application) {
    private val repository: ToDoRepository

    private val _allToDoItems = MutableLiveData<List<ToDoItem>>()
    val allToDoItems: LiveData<List<ToDoItem>> = _allToDoItems

    init {
        val dbHelper = DatabaseHelper(application)
        repository = ToDoRepository(dbHelper)
        _allToDoItems.postValue(repository.getAllToDoItems(listId))
    }

    fun addItem(name: String, completed: Boolean) {
        repository.addItem(name, completed, listId)
        _allToDoItems.postValue(repository.getAllToDoItems(listId))
    }

    fun deleteItem(list: ToDoItem) {
        repository.deleteItem(list)
        _allToDoItems.postValue(repository.getAllToDoItems(listId))
    }

    fun updateItem(list: ToDoItem) {
        repository.updateItem(list)
        _allToDoItems.postValue(repository.getAllToDoItems(listId))
    }

    fun getListNameById() :String {
        return repository.getListNameById(listId)
    }
}
