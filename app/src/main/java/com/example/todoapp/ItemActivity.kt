package com.example.todoapp

import android.os.Bundle
import android.text.InputType
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.adapter.ToDoItemAdapter
import com.example.todoapp.viewModel.ItemViewModelFactory
import com.example.todoapp.viewModel.ToDoItemViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ItemActivity : AppCompatActivity() {

    private lateinit var toDoItemAdapter: ToDoItemAdapter
    private lateinit var toDoItemViewModel: ToDoItemViewModel
    private var listId: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)

        listId = intent.getLongExtra("listId", -1)

        val itemRV = findViewById<RecyclerView>(R.id.todoItem)

        toDoItemViewModel = ViewModelProvider(this, ItemViewModelFactory(application, listId))[ToDoItemViewModel::class.java]

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = toDoItemViewModel.getListNameById()
        setSupportActionBar(toolbar)

        toDoItemAdapter = ToDoItemAdapter()
        itemRV.adapter = toDoItemAdapter
        itemRV.layoutManager = LinearLayoutManager(this)

        toDoItemAdapter.onItemDeleteClick = {
            val alertDialog = AlertDialog.Builder(this)
                .setTitle("Delete Item")
                .setMessage("Are you sure to delete this item?")
                .setPositiveButton("Delete") { _, _ ->
                    toDoItemViewModel.deleteItem(it)
                }
                .setNegativeButton("Cancel", null)
                .create()
            alertDialog.show()
        }

        toDoItemAdapter.onItemEditClick = {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Edit ToDo item")

            val layout = LinearLayout(this)
            layout.orientation = LinearLayout.VERTICAL
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            val margin = resources.getDimensionPixelSize(R.dimen.dialog_margin)
            layoutParams.setMargins(margin, margin, margin, margin)

            val editText = EditText(this)
            editText.inputType = InputType.TYPE_CLASS_TEXT
            editText.setText(it.name)
            layout.addView(editText, layoutParams)

            builder.setView(layout)

            builder.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }

            builder.setPositiveButton("Update") { _, _ ->
                val name = editText.text.toString().trim()
                if (name.isNotEmpty()) {
                    it.name = name
                    toDoItemViewModel.updateItem(it)
                } else {
                    Toast.makeText(this, "Please enter a name", Toast.LENGTH_SHORT)
                        .show()
                }
            }
            builder.create().show()
        }

        toDoItemViewModel.allToDoItems.observe(this) {
            toDoItemAdapter.submitItem(it)
        }

        val fab = findViewById<FloatingActionButton>(R.id.fab_item)

        fab.setOnClickListener {
            val builder = AlertDialog.Builder(this)

            builder.setTitle("Add ToDo item")

            val layout = LinearLayout(this)
            layout.orientation = LinearLayout.VERTICAL
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            val margin = resources.getDimensionPixelSize(R.dimen.dialog_margin)
            layoutParams.setMargins(margin, margin, margin, margin)

            val editText = EditText(this)
            editText.inputType = InputType.TYPE_CLASS_TEXT
            editText.hint = "Enter name"
            layout.addView(editText, layoutParams)

            builder.setView(layout)

            builder.setNegativeButton("Cancel") { dialog, _ ->
                dialog.dismiss()
            }

            builder.setPositiveButton("Add") { dialog, _ ->
                val name = editText.text.toString().trim()

                if (name.isNotEmpty()) {
                    toDoItemViewModel.addItem(name = name, completed = false)
                    dialog.dismiss()
                } else {
                    Toast.makeText(this, "Please enter a name", Toast.LENGTH_SHORT).show()
                }
            }

            builder.create().show()
        }
    }
}