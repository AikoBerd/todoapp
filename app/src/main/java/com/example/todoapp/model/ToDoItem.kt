package com.example.todoapp.model

data class ToDoItem(
    var id: Long = 0,
    var name: String,
    var completed: Boolean = false,
    var listId: Long
)