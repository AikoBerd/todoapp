package com.example.todoapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.model.ToDoList

class ToDoListAdapter :
    RecyclerView.Adapter<ToDoListAdapter.ListViewHolder>() {

    private val toDoList = mutableListOf<ToDoList>()

    var onListClick: ((ToDoList) -> Unit)? = null
    var onListEditClick: ((ToDoList) -> Unit)? = null
    var onListDeleteClick: ((ToDoList) -> Unit)? = null

    fun submitList(newList: List<ToDoList>) {
        toDoList.clear()
        toDoList.addAll(newList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.content_list, parent, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.onBind(toDoList[position])
    }

    override fun getItemCount(): Int = toDoList.size

    inner class ListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val toDoListName: TextView = view.findViewById(R.id.listName)
        private val listAction: ImageButton = view.findViewById(R.id.listAction)

        fun onBind(list: ToDoList){
            toDoListName.text = list.name
            itemView.setOnClickListener {
                onListClick?.invoke(list)
            }

            listAction.setOnClickListener {
                val popupMenu = PopupMenu(itemView.context, listAction)
                popupMenu.menuInflater.inflate(R.menu.menu, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener { menuItem ->
                    when (menuItem.itemId) {
                        R.id.edit_list -> {
                            onListEditClick?.invoke(list)
                            true
                        }

                        R.id.delete_list -> {
                            onListDeleteClick?.invoke(list)
                            true
                        }
                        else -> false
                    }
                }
                popupMenu.show()
            }
        }
    }
}