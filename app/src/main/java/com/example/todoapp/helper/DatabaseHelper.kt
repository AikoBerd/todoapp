package com.example.todoapp.helper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.todoapp.model.ToDoItem
import com.example.todoapp.model.ToDoList

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "todo_database"
        private const val DATABASE_VERSION = 1

        const val TODO_LIST_TABLE_NAME = "todo_list"
        const val TODO_LIST_COLUMN_ID = "_id"
        const val TODO_LIST_COLUMN_NAME = "name"

        const val TODO_ITEM_TABLE_NAME = "todo_item"
        const val TODO_ITEM_COLUMN_ID = "_id"
        const val TODO_ITEM_COLUMN_NAME = "name"
        const val TODO_ITEM_COLUMN_COMPLETED = "completed"
        const val TODO_ITEM_COLUMN_LIST_ID = "list_id"
    }

    override fun onCreate(db: SQLiteDatabase) {
        val createTodoListTable = "CREATE TABLE $TODO_LIST_TABLE_NAME (" +
                "$TODO_LIST_COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$TODO_LIST_COLUMN_NAME TEXT NOT NULL)"

        db.execSQL(createTodoListTable)

        val createTodoItemTable = "CREATE TABLE $TODO_ITEM_TABLE_NAME (" +
                "$TODO_ITEM_COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$TODO_ITEM_COLUMN_NAME TEXT NOT NULL, " +
                "$TODO_ITEM_COLUMN_COMPLETED INTEGER NOT NULL DEFAULT 0, " +
                "$TODO_ITEM_COLUMN_LIST_ID INTEGER NOT NULL, " +
                "FOREIGN KEY($TODO_ITEM_COLUMN_LIST_ID) REFERENCES $TODO_LIST_TABLE_NAME($TODO_LIST_COLUMN_ID))"

        db.execSQL(createTodoItemTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TODO_LIST_TABLE_NAME")
        db.execSQL("DROP TABLE IF EXISTS $TODO_ITEM_TABLE_NAME")
        onCreate(db)
    }

    fun addList(toDoList: ToDoList) {
        val values = ContentValues()
        values.put(TODO_LIST_COLUMN_NAME, toDoList.name)

        val db = this.writableDatabase
        db.insertWithOnConflict(TODO_LIST_TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE)
        db.close()
    }

    fun addItem(toDoItem: ToDoItem){
        val values = ContentValues()
        values.put(TODO_ITEM_COLUMN_NAME, toDoItem.name)
        values.put(TODO_ITEM_COLUMN_COMPLETED, toDoItem.completed)
        values.put(TODO_ITEM_COLUMN_LIST_ID, toDoItem.listId)

        val db = this.writableDatabase
        db.insertWithOnConflict(TODO_ITEM_TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE)
        Log.d("Database", "${toDoItem.id} and ${toDoItem.listId}")

        db.close()
    }

    fun deleteList(toDoList: ToDoList) {
        val db = this.writableDatabase
        db.delete(TODO_LIST_TABLE_NAME, "$TODO_LIST_COLUMN_ID=?", arrayOf(toDoList.id.toString()))
        db.close()
    }

    fun deleteItem(toDoItem: ToDoItem) {
        val db = this.writableDatabase
        db.delete(TODO_ITEM_TABLE_NAME, "$TODO_ITEM_COLUMN_ID=?", arrayOf(toDoItem.id.toString()))
        db.close()
    }


    fun updateList(toDoList: ToDoList) {
        val db = this.writableDatabase
        val values = ContentValues().apply {
            put(TODO_ITEM_COLUMN_NAME, toDoList.name)
        }
        db.update(TODO_LIST_TABLE_NAME, values, "$TODO_LIST_COLUMN_ID=?", arrayOf(toDoList.id.toString()))
        db.close()
    }

    fun updateItem(toDoItem: ToDoItem) {
        val db = this.writableDatabase
        val values = ContentValues().apply {
            put(TODO_ITEM_COLUMN_NAME, toDoItem.name)
            put(TODO_ITEM_COLUMN_COMPLETED, if (toDoItem.completed) 1 else 0)
            put(TODO_ITEM_COLUMN_LIST_ID, toDoItem.listId)
        }
        db.update(TODO_ITEM_TABLE_NAME, values, "$TODO_ITEM_COLUMN_ID=?", arrayOf(toDoItem.id.toString()))
        db.close()
    }

    fun getAllToDoLists(): List<ToDoList> {
        val toDoLists = mutableListOf<ToDoList>()
        val db = this.readableDatabase
        val columns = arrayOf(
            TODO_LIST_COLUMN_ID,
            TODO_LIST_COLUMN_NAME
        )
        val cursor = db.query(TODO_LIST_TABLE_NAME, columns, null, null, null, null, null)

        while (cursor.moveToNext()) {
            val id = cursor.getLong(cursor.getColumnIndexOrThrow(TODO_LIST_COLUMN_ID))
            val name = cursor.getString(cursor.getColumnIndexOrThrow(TODO_LIST_COLUMN_NAME))

            val toDoList = ToDoList(id, name)
            toDoLists.add(toDoList)
        }
        cursor.close()
        db.close()

        return toDoLists
    }

    fun getAllToDoItems(listId: Long): List<ToDoItem> {
        val toDoItems = mutableListOf<ToDoItem>()
        val db = readableDatabase
        val columns = arrayOf(
            TODO_ITEM_COLUMN_ID,
            TODO_ITEM_COLUMN_NAME,
            TODO_ITEM_COLUMN_COMPLETED,
            TODO_ITEM_COLUMN_LIST_ID
        )
        val selection = "$TODO_ITEM_COLUMN_LIST_ID = ?"
        val selectionArgs = arrayOf(listId.toString())
        val cursor = db.query(TODO_ITEM_TABLE_NAME, columns, selection, selectionArgs, null, null, null)

        while (cursor.moveToNext()) {
            val id = cursor.getLong(cursor.getColumnIndexOrThrow(TODO_ITEM_COLUMN_ID))
            val name = cursor.getString(cursor.getColumnIndexOrThrow(TODO_ITEM_COLUMN_NAME))
            val completed = cursor.getInt(cursor.getColumnIndexOrThrow(TODO_ITEM_COLUMN_COMPLETED)) != 0
            val listId = cursor.getLong(cursor.getColumnIndexOrThrow(TODO_ITEM_COLUMN_LIST_ID))

            val toDoItem = ToDoItem(id, name, completed, listId)
            toDoItems.add(toDoItem)
        }
        cursor.close()
        db.close()

        return toDoItems
    }

    fun getListNameById(listId: Long): String {
        val db = readableDatabase
        val columns = arrayOf(TODO_LIST_COLUMN_NAME)
        val selection = "$TODO_LIST_COLUMN_ID = ?"
        val selectionArgs = arrayOf(listId.toString())
        val cursor = db.query(TODO_LIST_TABLE_NAME, columns, selection, selectionArgs, null, null, null)

        val listName = if (cursor.moveToFirst()) {
            cursor.getString(cursor.getColumnIndexOrThrow(TODO_LIST_COLUMN_NAME))
        } else {
            ""
        }

        cursor.close()
        db.close()
        return listName
    }


}